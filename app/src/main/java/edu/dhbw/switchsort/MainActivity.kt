package edu.dhbw.switchsort

import android.media.AudioManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import edu.dhbw.switchsort.util.LoopMediaPlayer
import edu.dhbw.switchsort.util.SettingsHelper
import edu.dhbw.switchsort.util.ThemeHelper

class MainActivity : AppCompatActivity() {
    private var mediaPlayer: LoopMediaPlayer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hideSystemUI()
        setContentView(R.layout.activity_main)
        ThemeHelper(this).applyThemeFromPreferences()
        val appLinkIntent = intent
        val appLinkAction = appLinkIntent.action
        val appLinkData = appLinkIntent.data

        volumeControlStream = AudioManager.STREAM_MUSIC
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    @Suppress("Deprecation")
    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    // Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
    @Suppress("Deprecation")
    private fun showSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }

    override fun onStart() {
        super.onStart()
        if(SettingsHelper(this).isBackgroundMusicEnabled()) {
            startBackgroundMusic()
        }
    }

    override fun onStop() {
        super.onStop()
        stopBackgroundMusic()
    }

    fun startBackgroundMusic() {
        mediaPlayer = LoopMediaPlayer.create(this, R.raw.switchsort_ost, 0.85F)
        mediaPlayer?.start()

    }

    fun stopBackgroundMusic() {
        mediaPlayer?.stop()
        mediaPlayer?.release()
        mediaPlayer = null
    }
}