package edu.dhbw.switchsort.views

import android.animation.AnimatorInflater
import android.animation.LayoutTransition
import android.animation.ObjectAnimator
import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.core.view.get
import androidx.core.view.setPadding
import com.google.android.material.card.MaterialCardView
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.ExpandableCardviewBinding


@Suppress("RedundantNullableReturnType")
class ExpandableCardView(context: Context, attrs: AttributeSet) : MaterialCardView(context, attrs) {

    companion object {
        const val CARD_ICON_PADDING_DP = 12
    }

    private var binding: ExpandableCardviewBinding

    private var expanded:Boolean = false

    private var expandableContent: View

    private val clockwiseRotator: ObjectAnimator =
            AnimatorInflater.loadAnimator(context, R.animator.rotate_180_degrees_clockwise) as ObjectAnimator
    private val counterClockwiseRotator: ObjectAnimator =
            AnimatorInflater.loadAnimator(context, R.animator.rotate_180_degrees_counter_clockwise) as ObjectAnimator

    init {
        Log.i("init", "initInstanceState")
        val layoutInflater = LayoutInflater.from(context)
        binding = ExpandableCardviewBinding.inflate(layoutInflater, this, true)

        context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.ExpandableCardView,
                0,
                0
        ).apply {
            try {
                binding.cardTitle.text = getString(R.styleable.ExpandableCardView_titleText)
                binding.cardIcon.setImageDrawable(getDrawable(R.styleable.ExpandableCardView_icon))
                if(getBoolean(R.styleable.ExpandableCardView_iconBackgroundPadding, false)) {
                    binding.cardIcon.setPadding((CARD_ICON_PADDING_DP * resources.displayMetrics.density + 0.5f).toInt())
                }
                binding.cardSecondaryText.text = getString(R.styleable.ExpandableCardView_secondaryText)
                binding.cardContainer.addView(
                        LayoutInflater.from(context)
                                .inflate(
                                        getResourceId(R.styleable.ExpandableCardView_expandableContent, R.layout.card_content_information),
                                        binding.cardContainer,
                                        false
                                )
                )
                expandableContent = binding.cardContainer[1]
                setExpanded(false)
            } finally {
                recycle()
            }
        }
        binding.apply {
            chevronExpandCollapse.setOnClickListener {
                toggleExpandableContent()
            }
            clockwiseRotator.target = chevronExpandCollapse
            counterClockwiseRotator.target = chevronExpandCollapse
        }


        val cardContainerTransition: LayoutTransition = binding.cardContainer.layoutTransition
        cardContainerTransition.apply {
            enableTransitionType(LayoutTransition.APPEARING)
            enableTransitionType(LayoutTransition.DISAPPEARING)
            disableTransitionType(LayoutTransition.CHANGING)
            disableTransitionType(LayoutTransition.CHANGE_APPEARING)
        }
    }

    fun setTitleText(titleText: String) {
        binding.cardTitle.text = titleText
    }

    fun getTitleText(): String = binding.cardTitle.text.toString()

    fun setSecondaryText(secondaryText: String) {
        binding.cardSecondaryText.text = secondaryText
    }

    fun getSecondaryText() = binding.cardSecondaryText.text

    /**
     * Set this.expanded to the given value. Adjust the visibility of the expandable content and
     * the rotation of the chevron accordingly.
     */
    fun setExpanded(expanded: Boolean) {
        this.expanded = expanded
        if(expanded) {
            expandableContent.visibility = View.VISIBLE
            binding.chevronExpandCollapse.rotation = -180F
        } else {
            expandableContent.visibility = View.GONE
            binding.chevronExpandCollapse.rotation = 0F
        }

    }

    fun isExpanded() = expanded

    /**
     * Toggle this ExpandableCardView's expandable content.
     * Stop running animations, adjust the visibility of the expandable content and start a rotation
     * animation for the chevron.
     * Toggle this.expanded to be consistent with the  displayed state.
     */
    private fun toggleExpandableContent() {
        clockwiseRotator.end()
        counterClockwiseRotator.end()

        if (!expanded) {
            expandableContent.visibility = View.VISIBLE
            clockwiseRotator.start()
        } else {
            expandableContent.visibility = View.GONE
            counterClockwiseRotator.start()
        }
        expanded = !expanded
    }

    /**
     * Implement state saving for ExpandableCardView so it is automatically handled by the
     * Framework.
     * State to be saved is whether the card is expanded or collapsed.
     */
    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()

        val myState = SavedState(superState)
        myState.expanded = if(expanded) 1 else 0

        return myState
    }

    /**
     * Restore previously saved state from given Parcelable.
     * State to restore is whether the card is expanded or collapsed.
     * @param state: Parcelable? containing the state variables to restore.
     */
    override fun onRestoreInstanceState(state: Parcelable?) {
        val savedState = state as SavedState
        super.onRestoreInstanceState(savedState.superState)
        setExpanded(savedState.expanded == 1)
    }

    /**
     * Custom BaseSavedState implementation used to save and restore ExpandableCardView's state
     * in onSaveInstanceState() and onRestoresInstanceState.
     */
    private class SavedState : BaseSavedState {
        var expanded: Int = 0

        constructor(superState: Parcelable?) : super(superState)

        private constructor(`in`: Parcel) : super(`in`) {
            expanded = `in`.readInt()
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeInt(expanded)
        }

        companion object CREATOR: Parcelable.Creator<SavedState?> {
            override fun createFromParcel(`in`: Parcel): SavedState {
                return SavedState(`in`)
            }

            override fun newArray(size: Int): Array<SavedState?> {
                return arrayOfNulls(size)
            }
        }
    }

}