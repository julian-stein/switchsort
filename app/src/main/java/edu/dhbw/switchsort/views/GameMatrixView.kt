package edu.dhbw.switchsort.views

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.get
import androidx.core.view.iterator
import androidx.databinding.ViewDataBinding
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.GameMatrix3x3Binding
import edu.dhbw.switchsort.databinding.GameMatrix4x4Binding
import edu.dhbw.switchsort.databinding.GameMatrix5x5Binding
import edu.dhbw.switchsort.enums.MatrixSize
import edu.dhbw.switchsort.screens.game.GameViewModel
import edu.dhbw.switchsort.util.SettingsHelper

class GameMatrixView(context: Context, attrs: AttributeSet) :
        ConstraintLayout(context, attrs), View.OnClickListener {

    private lateinit var binding: ViewDataBinding
    private lateinit var matrixSize: MatrixSize
    private lateinit var matrix: ConstraintLayout
    private lateinit var gameViewModel: GameViewModel
    private lateinit var currentClickedButton: Button

    private val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    private val vibration = VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE)

    private val blueToRedAnimator: AnimatorSet =
            AnimatorInflater.loadAnimator(context, R.animator.blue_to_red) as AnimatorSet
    private val blueToGreenAnimator: AnimatorSet =
            AnimatorInflater.loadAnimator(context, R.animator.blue_to_green) as AnimatorSet

    fun setGameViewModelReference(gameViewModel: GameViewModel) {
        this.gameViewModel = gameViewModel
    }

    fun setIsHapticFeedbackEnabled() {
        isHapticFeedbackEnabled = SettingsHelper(context).isHapticFeedbackEnabled()
    }

    fun setLayout(matrixSize: MatrixSize) {
        this.matrixSize = matrixSize

        val layoutInflater = LayoutInflater.from(context)

        when (matrixSize) {
            MatrixSize.THREE -> {
                binding = GameMatrix3x3Binding.inflate(layoutInflater, this, true)
                matrix = (binding as GameMatrix3x3Binding).matrix
            }
            MatrixSize.FOUR -> {
                binding = GameMatrix4x4Binding.inflate(layoutInflater, this, true)
                matrix = (binding as GameMatrix4x4Binding).matrix
            }
            MatrixSize.FIVE -> {
                binding = GameMatrix5x5Binding.inflate(layoutInflater, this, true)
                matrix = (binding as GameMatrix5x5Binding).matrix
            }
        }

        for (v in matrix) {
            v.setOnClickListener(this)
        }

        invalidate()
        requestLayout()
    }

    fun setValues(list: List<Int>) {
        for (i in list.indices) {
            (matrix[i] as Button).text = list[i].toString()
        }
    }

    fun animateClicked(correctClick: Boolean) {
        if (correctClick) {
            blueToGreenAnimator.end()
            blueToGreenAnimator.setTarget(currentClickedButton.background as GradientDrawable)
            blueToGreenAnimator.start()
        } else {
            blueToRedAnimator.end()
            blueToRedAnimator.setTarget(currentClickedButton.background as GradientDrawable)
            blueToRedAnimator.start()
            if(isHapticFeedbackEnabled) {
                vibrator.vibrate(vibration)
            }
        }
    }

    override fun onClick(v: View?) {
        currentClickedButton = v as Button
        gameViewModel.checkForCorrectValue((v).text.toString().toInt())
    }

}