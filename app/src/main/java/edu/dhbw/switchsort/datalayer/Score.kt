package edu.dhbw.switchsort.datalayer

import java.time.LocalDate

/**
 * This class provides the information necessary to display a score in the scoreboard.
 * It is used to increase the efficiency when loading scores from the database by reducing the fields transferred.
 */
data class Score(
    val score: Float,
    val name: String,
    val date: LocalDate,
)