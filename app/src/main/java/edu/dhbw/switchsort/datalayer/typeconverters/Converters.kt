package edu.dhbw.switchsort.datalayer.typeconverters

import androidx.room.TypeConverter
import edu.dhbw.switchsort.enums.GameMode
import edu.dhbw.switchsort.enums.MatrixSize
import java.time.LocalDate

class Converters {
    @TypeConverter
    fun fromEpochDay(value: Long): LocalDate {
        return LocalDate.ofEpochDay(value)
    }

    @TypeConverter
    fun localDateToEpochDay(localDate: LocalDate): Long {
        return localDate.toEpochDay()
    }

    @TypeConverter
    fun fromStringGameMode(value: String): GameMode {
        return GameMode.valueOf(value)
    }

    @TypeConverter
    fun gameModeToString(gameMode: GameMode): String {
        return gameMode.name
    }

    @TypeConverter
    fun fromIntMatrixSize(value: Int): MatrixSize {
        return MatrixSize.getByValue(value)
    }

    @TypeConverter
    fun matrixSizeToString(matrixSize: MatrixSize): Int {
        return matrixSize.value
    }


}