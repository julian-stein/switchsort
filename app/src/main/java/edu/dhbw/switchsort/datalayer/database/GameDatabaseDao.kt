package edu.dhbw.switchsort.datalayer.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import edu.dhbw.switchsort.datalayer.Score
import edu.dhbw.switchsort.datalayer.entities.Game
import edu.dhbw.switchsort.enums.GameMode
import edu.dhbw.switchsort.enums.MatrixSize

@Dao
interface GameDatabaseDao {
    @Insert
    fun insert(game: Game)

    @Update
    fun update(game: Game)

    @Query("delete from Game")
    fun clear()

    @Query("select score, name, date from Game where name is not null and game_mode = :gameMode and matrix_size = :matrixSize and finished = 1 order by score desc")
    fun getScoresForScoreboardDisplayByGameModeAndMatrixSizeOrderByScoreDesc(gameMode: GameMode, matrixSize: MatrixSize): List<Score>

    @Query("select * from Game where finished = 0 order by game_id desc limit 1")
    fun getLoadableGame(): LiveData<Game?>

    @Query("select * from Game where finished = 0 order by game_id desc limit 1")
    fun getCurrentGame(): Game?

    @Query("delete from Game where finished = 0")
    fun deleteAllUnfinishedGames()

    @Query("delete from Game where game_id = (select max(game_id) from Game)")
    fun deleteMostRecentFinishedGame(): Int

    @Query("select * from Game where finished = 1 order by game_id desc limit 1")
    fun getMostRecentFinishedGame(): Game?

    @Query("select * from Game where finished = 1 and name is not null order by game_id desc limit 1")
    fun getMostRecentFinishedGameWithName(): Game?

    @Query("select * from Game order by game_id desc limit 1")
    fun getMostRecentGame(): Game?

    @Query("delete from Game where finished = 1 and name is null")
    fun deleteAllFinishedGamesWithNoName()

    @Query("delete from Game where finished = 1 and name is null and game_id != (select max(game_id) from Game)")
    fun deleteAllFinishedGamesWithNoNameExceptLatest()
}