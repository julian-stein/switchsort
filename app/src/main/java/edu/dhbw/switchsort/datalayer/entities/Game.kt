package edu.dhbw.switchsort.datalayer.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import edu.dhbw.switchsort.enums.GameMode
import edu.dhbw.switchsort.enums.MatrixSize
import java.time.LocalDate
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class Game(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "game_id")
        var gameId: Long,

        @ColumnInfo(name = "game_mode")
        var gameMode: GameMode,

        @ColumnInfo(name = "matrix_size")
        var matrixSize: MatrixSize,

        @ColumnInfo(name = "milli_seconds_left")
        var milliSecondsLeft: Long?,

        @ColumnInfo(name = "lives")
        var lives: Int?,

        @ColumnInfo(name = "score")
        var score: Float = 0F,

        @ColumnInfo(name = "name")
        var name: String?,

        @ColumnInfo(name = "date")
        var date: LocalDate,

        @ColumnInfo(name = "finished")
        var finished: Boolean = false
) : Parcelable
