package edu.dhbw.switchsort.screens.setup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentSetupBinding
import edu.dhbw.switchsort.datalayer.database.GameDatabase

class SetupFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        val binding: FragmentSetupBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_setup, container, false)

        val application = requireNotNull(this.activity).application
        val dao = GameDatabase.getInstance(application).gameDatabaseDao

        val setupViewModel: SetupViewModel = ViewModelProvider(
                this,
                SetupViewModelFactory(dao, this, null)
        ).get(SetupViewModel::class.java)

        setupViewModel.navigateToGame.observe(viewLifecycleOwner, {
            if (it) {
                findNavController().navigate(SetupFragmentDirections.actionSetupFragmentToGameFragment())
                setupViewModel.doneNavigating()
            }
        })

        binding.goBackButton.setOnClickListener  {
            findNavController().navigateUp()
        }

        binding.lifecycleOwner = viewLifecycleOwner
        binding.setupViewModel = setupViewModel

        return binding.root
    }


}