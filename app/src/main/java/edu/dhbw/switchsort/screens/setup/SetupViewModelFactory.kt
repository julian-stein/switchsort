package edu.dhbw.switchsort.screens.setup

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import edu.dhbw.switchsort.datalayer.database.GameDatabaseDao


class SetupViewModelFactory(
    private val dao: GameDatabaseDao,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle?
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T {
        if (modelClass.isAssignableFrom(SetupViewModel::class.java)) {
            return SetupViewModel(dao, handle) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}