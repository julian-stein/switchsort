package edu.dhbw.switchsort.screens.title

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentTitleBinding
import edu.dhbw.switchsort.datalayer.database.GameDatabase


class TitleFragment : Fragment() {

    private lateinit var titleViewModel: TitleViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        val binding: FragmentTitleBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_title, container, false)

        val application = requireNotNull(this.activity).application
        val dao = GameDatabase.getInstance(application).gameDatabaseDao

        val viewModelFactory = TitleViewModelFactory(dao)

        titleViewModel =
                ViewModelProvider(this, viewModelFactory)
                        .get(TitleViewModel::class.java)

        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            titleViewModel = this@TitleFragment.titleViewModel

            newGameButton.setOnClickListener {
                findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToSetupFragment())
            }
            scoreBoardButton.setOnClickListener {
                findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToScoreboardFragment())
            }
            loadGameButton.setOnClickListener {
                findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToGameFragment())
            }
            openSettingsFloatingButton.setOnClickListener {
                findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToSettingsFragment())
            }
            openAboutFloatingButton.setOnClickListener {
                findNavController().navigate(TitleFragmentDirections.actionTitleFragmentToAboutFragment())
            }
        }

        return binding.root
    }
}