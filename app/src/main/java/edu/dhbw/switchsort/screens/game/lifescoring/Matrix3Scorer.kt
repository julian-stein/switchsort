package edu.dhbw.switchsort.screens.game.lifescoring

import kotlin.math.pow

class Matrix3Scorer: LifeScorer() {
    override fun getLifeScore(clickTime: Long): Float {
        val x = ((((clickTime.toFloat()/1000) * 0.826 - 0.31).pow(-1)) - 0.283).toFloat()
        if (x > 3F) {
            return 3F
        } else if (x < 0.1F) {
            return 0.1F
        }
        return x
    }
}