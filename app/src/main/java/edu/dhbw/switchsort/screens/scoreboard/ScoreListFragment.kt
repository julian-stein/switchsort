package edu.dhbw.switchsort.screens.scoreboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.datalayer.database.GameDatabase
import edu.dhbw.switchsort.enums.MatrixSize


/**
 * A fragment representing a list of Items.
 */
class ScoreListFragment : Fragment() {

    private lateinit var matrixSize: MatrixSize

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        matrixSize = MatrixSize.getByValue(requireArguments().getInt(ARG_MATRIX_SIZE))

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: RecyclerView =
            inflater.inflate(R.layout.fragment_score_list, container, false) as RecyclerView

        view.addItemDecoration(DividerItemDecoration(view.context, DividerItemDecoration.VERTICAL))
        val adapter = ScoreListRecyclerViewAdapter()
        view.adapter = adapter


        val application = requireNotNull(this.activity).application

        val dao = GameDatabase.getInstance(application).gameDatabaseDao

        val viewModelFactory = ScoreboardViewModelFactory(dao, requireParentFragment(), null)

        val scoreboardViewModel =
            ViewModelProvider(requireParentFragment(), viewModelFactory)
                .get(ScoreboardViewModel::class.java)

        scoreboardViewModel.scores[matrixSize]!!.observe(viewLifecycleOwner, {
            it?.let {
                adapter.scores = it
                view.scrollToPosition(0)
            }
        })

        return view
    }

    companion object {
        const val ARG_MATRIX_SIZE = "matrix_size"

        @JvmStatic
        fun newInstance(matrixSize: MatrixSize) =
            ScoreListFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_MATRIX_SIZE, matrixSize.value)
                }
            }
    }
}