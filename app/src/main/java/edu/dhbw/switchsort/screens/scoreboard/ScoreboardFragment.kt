package edu.dhbw.switchsort.screens.scoreboard

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentScoreboardBinding
import com.google.android.material.tabs.TabLayoutMediator
import edu.dhbw.switchsort.datalayer.database.GameDatabase
import edu.dhbw.switchsort.enums.MatrixSize

class ScoreboardFragment : Fragment() {

    companion object {
        const val MATRIX_SIZE_OFFSET = 3
    }

    private lateinit var binding: FragmentScoreboardBinding
    private lateinit var scoreboardViewModel: ScoreboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_scoreboard,
            container,
            false
        )

        binding.goBackButton.setOnClickListener {
            findNavController().navigateUp()
        }

        val application = requireNotNull(this.activity).application

        val dao = GameDatabase.getInstance(application).gameDatabaseDao

        val viewModelFactory = ScoreboardViewModelFactory(dao, this, null)

        scoreboardViewModel =
            ViewModelProvider(this, viewModelFactory)
                .get(ScoreboardViewModel::class.java)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.scoreboardViewModel = scoreboardViewModel

        scoreboardViewModel.matrixSize.observe(viewLifecycleOwner, {
            if (it != null) {
                binding.scoreListsViewPager.currentItem = it.value - MATRIX_SIZE_OFFSET
            }
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.scoreListsViewPager.adapter = ScoreboardViewPagerAdapter(this@ScoreboardFragment)

        TabLayoutMediator(binding.matrixTabLayout, binding.scoreListsViewPager) { tab, position ->
            tab.text = "${position + MATRIX_SIZE_OFFSET}x${position + MATRIX_SIZE_OFFSET}"
            tab.icon = ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_apps_default,
                requireActivity().theme
            )
        }.attach()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        scoreboardViewModel.saveSelectedMatrixSize(
            MatrixSize.getByValue(
                binding.scoreListsViewPager.currentItem + MATRIX_SIZE_OFFSET
            )
        )
    }
}