package edu.dhbw.switchsort.screens.scoreboard

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import edu.dhbw.switchsort.databinding.ListItemScoreBinding
import edu.dhbw.switchsort.datalayer.Score

class ScoreListRecyclerViewAdapter()
    : RecyclerView.Adapter<ScoreListRecyclerViewAdapter.ScoreViewHolder>() {

    var scores = listOf<Score>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoreViewHolder {
        return ScoreViewHolder.from( parent)
    }

    override fun onBindViewHolder(holder: ScoreViewHolder, position: Int) {
        val item = scores[position]
        holder.bind(item, position)
    }

    override fun getItemCount(): Int = scores.size

    class ScoreViewHolder private constructor(val binding: ListItemScoreBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Score, position: Int) {
            binding.placementValue = (position + 1L).toString()
            binding.score = item
        }

        companion object {
            fun from(parent: ViewGroup): ScoreViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemScoreBinding.inflate(layoutInflater, parent, false)
                return ScoreViewHolder(binding)
            }
        }
    }
}