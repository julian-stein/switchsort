package edu.dhbw.switchsort.screens.settings

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentDayNightSelectionBinding
import edu.dhbw.switchsort.databinding.FragmentGameSettingsBinding
import edu.dhbw.switchsort.util.SettingsHelper

/**
 * A Fragment subclass providing switches for the user to toggle game-relevant settings.
 * Toggling triggers saving the new preference to shared preferences.
 */
class GameSettingsFragment : Fragment() {

    private lateinit var settingsHelper: SettingsHelper

    override fun onAttach(context: Context) {
        super.onAttach(context)
        settingsHelper = SettingsHelper(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentGameSettingsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_game_settings, container, false)

        binding.apply { 
            hapticFeedbackSwitch.isChecked = settingsHelper.isHapticFeedbackEnabled()
            hapticFeedbackSwitch.setOnCheckedChangeListener { _, isChecked ->
                settingsHelper.setHapticFeedbackEnabled(isChecked)
            }

            acousticFeedbackSwitch.isChecked = settingsHelper.isAcousticFeedbackEnabled()
            acousticFeedbackSwitch.setOnCheckedChangeListener { _, isChecked ->
                settingsHelper.setAcousticFeedbackEnabled(isChecked)
            }
        }

        return binding.root
    }
}