package edu.dhbw.switchsort.screens.settings

import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentDayNightSelectionBinding
import edu.dhbw.switchsort.util.ThemeHelper

/**
 * A Fragment subclass providing a radio group consisting of two or three radio buttons, depending
 * on build version, for the user to change the UI mode. Changing the selected button triggers
 * saving the new preference to shared preferences and the UI to adapt to the selected mode.
 */
class DayNightSelectionFragment : Fragment() {

    private lateinit var themeHelper: ThemeHelper

    override fun onAttach(context: Context) {
        super.onAttach(context)
        themeHelper = ThemeHelper(requireContext())
    }

    /**
     * Inflate this fragment's layout.
     * Adjust the layout to show (default) or hide the "Follow System" option depending on build
     * version.
     * Check the radio button representing the stored preference.
     * Add an onCheckedChangeListener to the radio group to handle button selection.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentDayNightSelectionBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_day_night_selection, container, false)

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            binding.followSystemModeRadioButton.visibility = View.GONE
        }

        binding.dayNightRadioGroup.check(when(themeHelper.getThemeFromPreferences()) {
            AppCompatDelegate.MODE_NIGHT_YES -> R.id.nightModeRadioButton
            AppCompatDelegate.MODE_NIGHT_NO -> R.id.lightModeRadioButton
            else -> R.id.followSystemModeRadioButton
        })
        
        binding.dayNightRadioGroup.setOnCheckedChangeListener { group, checkedId ->
            themeHelper.apply {
                saveThemeToPreferences(when(checkedId) {
                    R.id.nightModeRadioButton -> AppCompatDelegate.MODE_NIGHT_YES
                    R.id.lightModeRadioButton -> AppCompatDelegate.MODE_NIGHT_NO
                    else -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
                })
                applyThemeFromPreferences()
            }
        }

        return binding.root
    }
}