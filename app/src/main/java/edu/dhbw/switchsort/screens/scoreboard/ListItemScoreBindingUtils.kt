package edu.dhbw.switchsort.screens.scoreboard

import android.widget.TextView
import androidx.databinding.BindingAdapter
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.datalayer.Score
import java.time.format.DateTimeFormatter

@BindingAdapter("scoreboardMetaData")
fun TextView.setScoreboardMetadataFormatted(item: Score) {
    text = context.resources.getString(
            R.string.scoreboardMetadata,
            item.name,
            item.date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
    )
}

@BindingAdapter("scoreboardScore")
fun TextView.setScoreboardScoreFormatted(item: Score) {
    text = String.format(java.util.Locale.US, "%.1f", item.score)
}