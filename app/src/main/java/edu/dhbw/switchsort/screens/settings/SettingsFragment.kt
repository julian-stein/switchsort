package edu.dhbw.switchsort.screens.settings

import android.animation.LayoutTransition
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.navigation.fragment.findNavController
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentSettingsBinding


class SettingsFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val binding: FragmentSettingsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_settings, container, false)

        binding.goBackButton.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.settingsList.layoutTransition.apply {
            enableTransitionType(LayoutTransition.CHANGING)
            disableTransitionType(LayoutTransition.APPEARING)
            disableTransitionType(LayoutTransition.CHANGE_APPEARING)
            disableTransitionType(LayoutTransition.DISAPPEARING)
        }

        if(savedInstanceState == null) {
            childFragmentManager.commit {
                setReorderingAllowed(true)
                add<DayNightSelectionFragment>(R.id.dayNightSelectorFragment)
                add<BackgroundMusicSettingFragment>(R.id.backgroundMusicSettingFragment)
                add<GameSettingsFragment>(R.id.gameSettingsFragment)
            }
        }

        return binding.root
    }
}