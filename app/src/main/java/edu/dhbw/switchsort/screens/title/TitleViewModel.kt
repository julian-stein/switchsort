package edu.dhbw.switchsort.screens.title

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import edu.dhbw.switchsort.datalayer.database.GameDatabaseDao
import edu.dhbw.switchsort.datalayer.entities.Game

class TitleViewModel(
        private val gameDao: GameDatabaseDao) : ViewModel() {

    private val loadableGame: LiveData<Game?> = gameDao.getLoadableGame()
    val loadableGameAvailable: LiveData<Boolean> = Transformations.map(loadableGame) {
        it != null
    }

}