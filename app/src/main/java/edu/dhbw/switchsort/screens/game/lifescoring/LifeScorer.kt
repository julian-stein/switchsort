package edu.dhbw.switchsort.screens.game.lifescoring

abstract class LifeScorer {
    abstract fun getLifeScore(clickTime: Long): Float
}