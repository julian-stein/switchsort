package edu.dhbw.switchsort.screens.game

import android.app.Application
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.datalayer.database.GameDatabaseDao
import edu.dhbw.switchsort.datalayer.entities.Game
import edu.dhbw.switchsort.enums.GameMode
import edu.dhbw.switchsort.enums.MatrixSize
import edu.dhbw.switchsort.screens.game.lifescoring.LifeScorer
import edu.dhbw.switchsort.screens.game.lifescoring.Matrix3Scorer
import edu.dhbw.switchsort.screens.game.lifescoring.Matrix4Scorer
import edu.dhbw.switchsort.screens.game.lifescoring.Matrix5Scorer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class GameViewModel(
    private val gameDao: GameDatabaseDao,
    application: Application,
) : ViewModel() {

    companion object {
        const val PENALTY_TIME_TIMER_MODE = 5000L
    }

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var timer: GameTimer? = null

    private lateinit var lifeScorer: LifeScorer

    private val lifeFullDrawable = ContextCompat.getDrawable(application, R.drawable.ic_life_full)!!
    private val lifeTwoThirdDrawable =
        ContextCompat.getDrawable(application, R.drawable.ic_life_two_third)!!
    private val lifeOneThirdDrawable =
        ContextCompat.getDrawable(application, R.drawable.ic_life_one_third)!!

    private var storedGameOnNavigation: Boolean = false

    private var measureStartTime: Long = System.currentTimeMillis()
    private var measureEndTime: Long = System.currentTimeMillis()
    private var measureDelta: Long = 0L

    private var clickCounter: Int = 0

    private val _game: MutableLiveData<Game> = MutableLiveData()
    val game: LiveData<Game>
        get() = _game

    private val _matrixFieldsValues: MutableLiveData<List<Int>> = MutableLiveData()
    val matrixFieldsValues: LiveData<List<Int>>
        get() = _matrixFieldsValues

    private val _currentValue: MutableLiveData<Int> = MutableLiveData()
    val currentValue: LiveData<Int>
        get() = _currentValue

    val currentValueString = Transformations.map(_currentValue) {
        it.toString()
    }

    private val _currentScore = MutableLiveData<String>()
    val currentScore: LiveData<String>
        get() = _currentScore

    private val _currentTimerMillis = MutableLiveData<Long>()
    val currentTimerMillis: LiveData<Long>
        get() = _currentTimerMillis

    val currentTimer: LiveData<String> = Transformations.map(currentTimerMillis) {
        if (it != null) {
            val minutes = _currentTimerMillis.value?.div((60 * 1000))
            val seconds = (_currentTimerMillis.value?.div(1000) ?: 0) % 60
            String.format("%d:%02d", minutes, seconds)
        } else {
            ""
        }
    }

    private val _currentLifeDrawable = MutableLiveData<Drawable>()
    val currentLifeDrawable: LiveData<Drawable>
        get() = _currentLifeDrawable

    private val _navigateToResults = MutableLiveData<Boolean>()
    val navigateToResults: LiveData<Boolean>
        get() = _navigateToResults

    private val _correctClick = MutableLiveData<Boolean?>()
    val correctClick: LiveData<Boolean?>
        get() = _correctClick


    init {
        _currentTimerMillis.value = 90000
        _currentScore.value = "0.00"
        _matrixFieldsValues.value = listOf()
        _game.value = null
        onStartup()
    }

    private fun onStartup() {
        uiScope.launch {
            _game.value = loadGame() ?: return@launch
            setStartValues()
            lifeScorer = when(_game.value!!.matrixSize) {
                MatrixSize.THREE -> Matrix3Scorer()
                MatrixSize.FOUR -> Matrix4Scorer()
                MatrixSize.FIVE -> Matrix5Scorer()
            }
        }
    }

    private fun setStartValues() {
        _currentScore.value = String.format(Locale.US,"%.1f", _game.value?.score)
        measureStartTime = System.currentTimeMillis()

        updateBoardValues()
        if (_game.value?.gameMode == GameMode.TIMER) {
            _currentTimerMillis.value = _game.value?.milliSecondsLeft!!
            startGameTimer()
        } else {
            _currentLifeDrawable.value = when (_game.value?.lives) {
                3 -> lifeFullDrawable
                2 -> lifeTwoThirdDrawable
                else -> lifeOneThirdDrawable
            }
        }
    }

    private suspend fun loadGame(): Game? {
        return withContext(Dispatchers.IO) {
            gameDao.getCurrentGame()
        }
    }

    fun checkForCorrectValue(value: Int) {
        clickCounter++
        _correctClick.value = value == _currentValue.value
        updateBoardValues()
    }

    private fun updateBoardValues() {
        measureEndTime  = System.currentTimeMillis()
        if(measureEndTime != measureStartTime) {
            measureDelta = measureEndTime-measureStartTime
            measureStartTime = System.currentTimeMillis()
        }
        extractRandomValueAndShuffleList()
        calculateScore()
        if (_game.value?.gameMode == GameMode.LIFE) {
            manageGameLives()
        } else if(_game.value?.gameMode == GameMode.TIMER && _correctClick.value == false) {
            timer?.reduceTimeLeft(PENALTY_TIME_TIMER_MODE)
        }
    }

    private fun extractRandomValueAndShuffleList() {
        if (_game.value != null) {
            val newList = generateFieldList(_game.value!!.matrixSize)
            _currentValue.value = newList.random()
            _matrixFieldsValues.value = newList
        }
    }

    private fun calculateScore() {
        if (_correctClick.value == true) {
            if(_game.value?.gameMode == GameMode.TIMER) {
                _game.value?.score = _game.value?.score?.plus(1)!!
                _currentScore.value = String.format(Locale.US,"%.1f", _game.value?.score)
            } else {
                _game.value?.score = _game.value?.score?.plus(lifeScorer.getLifeScore(measureDelta))!!
                _currentScore.value = String.format(Locale.US, "%.1f", _game.value?.score)
            }
        }
    }

    private fun startGameTimer() {
        timer = object : GameTimer(_game.value?.milliSecondsLeft!!, 1000, true) {
            override fun onTick(millisUntilFinished: Long) {
                _currentTimerMillis.value = millisUntilFinished
                _game.value?.milliSecondsLeft = millisUntilFinished
            }

            override fun onFinish() {
                _game.value?.finished = true
                onNavigateToResultsView()
            }
        }
        (timer as GameTimer).create()
    }

    fun pauseGame() {
        if (timer != null && timer!!.isRunning) {
            timer?.pause()
        }
    }

    fun resumeGame() {
        extractRandomValueAndShuffleList()
        measureStartTime = System.currentTimeMillis()
        if (timer != null && timer!!.isPaused) {
            timer?.resume()
        }
    }

    private fun manageGameLives() {
        if (_correctClick.value != null && !_correctClick.value!!) {
            _game.value?.lives = _game.value?.lives?.minus(1)
            when (_game.value?.lives) {
                3 -> _currentLifeDrawable.value = lifeFullDrawable
                2 -> _currentLifeDrawable.value = lifeTwoThirdDrawable
                1 -> _currentLifeDrawable.value = lifeOneThirdDrawable
            }
        }

        if (game.value?.lives == 0) {
            _game.value?.finished = true
            onNavigateToResultsView()
        }
    }

    fun resetCorrectClick() {
        _correctClick.value = null
    }

    private suspend fun storeGameToDatabase() {
        if (_game.value != null) {
            withContext(Dispatchers.IO) {
                gameDao.update(_game.value!!)
            }
        }
    }

    fun storeGame() {
        uiScope.launch {
            storeGameToDatabase()
        }
    }

    private fun generateFieldList(matrixSize: MatrixSize): MutableList<Int> {
        val list = (1..99).toMutableList()
        list.shuffle()
        return list.subList(0, matrixSize.value * matrixSize.value)
    }

    fun doneNavigating() {
        _navigateToResults.value = false
    }

    fun onNavigateToResultsView() {
        uiScope.launch {
            storeGameToDatabase()
            storedGameOnNavigation = true
            _navigateToResults.value = true
        }
    }

    override fun onCleared() {
        super.onCleared()
        timer?.cancel()
        viewModelJob.cancel()
    }
}