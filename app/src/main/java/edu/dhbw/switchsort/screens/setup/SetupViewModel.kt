package edu.dhbw.switchsort.screens.setup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import edu.dhbw.switchsort.datalayer.database.GameDatabaseDao
import edu.dhbw.switchsort.datalayer.entities.Game
import edu.dhbw.switchsort.enums.GameMode
import edu.dhbw.switchsort.enums.MatrixSize
import kotlinx.coroutines.*
import java.time.LocalDate

class SetupViewModel(
    private val gameDao: GameDatabaseDao,
    private val state: SavedStateHandle
) : ViewModel() {

    companion object {
        const val RESTORING_STATE_KEY = "restoring_state"
        const val GAME_MODE_KEY = "game_mode"
        const val MATRIX_SIZE_KEY = "matrix_size"
    }

    private var viewModelJob = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val gameMode: LiveData<GameMode> = state.getLiveData(GAME_MODE_KEY)

    val isSelectedGameModeLife: LiveData<Boolean> = Transformations.map(gameMode) {
        it.equals(GameMode.LIFE)
    }

    val isSelectedGameModeTimer: LiveData<Boolean> = Transformations.map(gameMode) {
        it.equals(GameMode.TIMER)
    }

    private val matrixSize: LiveData<MatrixSize> = state.getLiveData(MATRIX_SIZE_KEY)

    val isSelectedMatrixSizeThree: LiveData<Boolean> = Transformations.map(matrixSize) {
        it.equals(MatrixSize.THREE)
    }

    val isSelectedMatrixSizeFour: LiveData<Boolean> = Transformations.map(matrixSize) {
        it.equals(MatrixSize.FOUR)
    }

    val isSelectedMatrixSizeFive: LiveData<Boolean> = Transformations.map(matrixSize) {
        it.equals(MatrixSize.FIVE)
    }

    private val _navigateToGame = MutableLiveData<Boolean>()
    val navigateToGame: LiveData<Boolean>
        get() = _navigateToGame

    init {
        if (!state.contains(RESTORING_STATE_KEY)) {
            state.set(RESTORING_STATE_KEY, true)
            uiScope.launch {
                var lastGame: Game
                withContext(Dispatchers.IO) {
                    lastGame = gameDao.getMostRecentGame() ?: Game(
                        0L,
                        GameMode.LIFE,
                        MatrixSize.THREE,
                        null,
                        null,
                        0F,
                        null,
                        LocalDate.now(),
                        false
                    )
                }
                state.set(GAME_MODE_KEY, lastGame.gameMode)
                state.set(MATRIX_SIZE_KEY, lastGame.matrixSize)
            }
        }
    }

    fun onSelectTimerMode() {
        state.set(GAME_MODE_KEY, GameMode.TIMER)
    }

    fun onSelectLifeMode() {
        state.set(GAME_MODE_KEY, GameMode.LIFE)
    }

    fun onSelectMatrix3() {
        state.set(MATRIX_SIZE_KEY, MatrixSize.THREE)
    }

    fun onSelectMatrix4() {
        state.set(MATRIX_SIZE_KEY, MatrixSize.FOUR)
    }

    fun onSelectMatrix5() {
        state.set(MATRIX_SIZE_KEY, MatrixSize.FIVE)
    }

    fun doneNavigating() {
        _navigateToGame.value = false
    }

    fun onNavigateToGameView() {
        val game = Game(
            0L,
            GameMode.LIFE,
            MatrixSize.THREE,
            null,
            null,
            0F,
            null,
            LocalDate.now(),
            false
        )
        game.gameMode = state.get(GAME_MODE_KEY)!!
        game.matrixSize = state.get(MATRIX_SIZE_KEY)!!
        if (game.gameMode == GameMode.TIMER) {
            game.milliSecondsLeft = 90000
        } else {
            game.lives = 3
        }
        uiScope.launch {
            storeGame(game)
            cleanDatabase()
            _navigateToGame.value = true
        }
    }

    private suspend fun cleanDatabase() {
        withContext(Dispatchers.IO) {
            gameDao.deleteAllFinishedGamesWithNoName()
        }
    }
    private suspend fun storeGame(game: Game) {
        withContext(Dispatchers.IO) {
            gameDao.deleteAllUnfinishedGames()
            gameDao.insert(game)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}