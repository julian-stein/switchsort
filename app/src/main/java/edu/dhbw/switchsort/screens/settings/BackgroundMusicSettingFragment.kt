package edu.dhbw.switchsort.screens.settings

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import edu.dhbw.switchsort.MainActivity
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentBackgroundMusicSettingsBinding
import edu.dhbw.switchsort.util.SettingsHelper

/**
 * A Fragment subclass providing a switch for the user to toggle the background music in the app.
 * Toggling triggers saving the new preference to shared preferences and starts or stop
 * the music by calling the MainActivity's functions.
 */
class BackgroundMusicSettingFragment : Fragment() {

    private lateinit var settingsHelper: SettingsHelper

    override fun onAttach(context: Context) {
        super.onAttach(context)
        settingsHelper = SettingsHelper(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentBackgroundMusicSettingsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_background_music_settings, container, false)

        binding.apply {
            backgroundMusicSwitch.isChecked = settingsHelper.isBackgroundMusicEnabled()
            backgroundMusicSwitch.setOnCheckedChangeListener { _, isChecked ->
                settingsHelper.setBackgroundMusicEnabled(isChecked)
                if(isChecked) {
                    (requireActivity() as MainActivity).startBackgroundMusic()
                } else {
                    (requireActivity() as MainActivity).stopBackgroundMusic()
                }
            }
        }

        return binding.root
    }
}