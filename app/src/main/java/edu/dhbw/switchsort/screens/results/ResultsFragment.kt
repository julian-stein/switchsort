package edu.dhbw.switchsort.screens.results

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentResultsBinding
import edu.dhbw.switchsort.datalayer.database.GameDatabase
import edu.dhbw.switchsort.datalayer.entities.Game
import edu.dhbw.switchsort.enums.GameMode

class ResultsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val binding: FragmentResultsBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_results, container, false
        )

        val application = requireNotNull(this.activity).application
        val dao = GameDatabase.getInstance(application).gameDatabaseDao

        val resultsViewModel: ResultsViewModel = ViewModelProvider(
            this,
            ResultsViewModelFactory(dao, this, null)
        ).get(ResultsViewModel::class.java)

        binding.insertNameInnerTextField.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    resultsViewModel.storeGameWithNameIfSet(binding.insertNameInnerTextField.text)
                    false
                }
                else -> false
            }
        }

        binding.resultsViewModel = resultsViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        binding.goBackButton.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.playAgainButton.setOnClickListener {
            findNavController().navigate(ResultsFragmentDirections.actionResultsFragmentToSetupFragment())
        }
        binding.titleButton.setOnClickListener {
            findNavController().navigate(ResultsFragmentDirections.actionResultsFragmentToTitleFragment())
        }
        binding.scoreboardButtonResults.setOnClickListener {
            findNavController().navigate(ResultsFragmentDirections.actionResultsFragmentToScoreboardFragment())
        }
        binding.shareResultsButton.setOnClickListener {
            createShareResultsIntent(resultsViewModel.game)
        }
        return binding.root
    }

    private fun createShareResultsIntent(game: Game?) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(
                Intent.EXTRA_TEXT,
                resources.getString(
                    R.string.shareText,
                    if (game?.gameMode == GameMode.TIMER)
                        resources.getString(
                            R.string.timeString
                        )
                    else
                        resources.getString(
                            R.string.lifeString
                        ),
                    game?.matrixSize?.value,
                    game?.score
                )
            )
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }
}