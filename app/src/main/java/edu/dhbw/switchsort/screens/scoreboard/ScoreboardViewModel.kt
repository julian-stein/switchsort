package edu.dhbw.switchsort.screens.scoreboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import edu.dhbw.switchsort.datalayer.Score
import edu.dhbw.switchsort.datalayer.database.GameDatabaseDao
import edu.dhbw.switchsort.datalayer.entities.Game
import edu.dhbw.switchsort.enums.GameMode
import edu.dhbw.switchsort.enums.MatrixSize
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDate

class ScoreboardViewModel(
    private val gameDao: GameDatabaseDao,
    private val state: SavedStateHandle
) : ViewModel() {

    companion object {
        private const val RESTORING_STATE_KEY = "restoring_state"
        private const val GAME_MODE_KEY = "game_mode"
        private const val MATRIX_SIZE_KEY = "matrix_size"
    }

    private var viewModelJob = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val gameMode: LiveData<GameMode> = state.getLiveData(GAME_MODE_KEY)

    val isSelectedGameModeLife: LiveData<Boolean> = Transformations.map(gameMode) {
        it.equals(GameMode.LIFE)
    }

    val isSelectedGameModeTimer: LiveData<Boolean> = Transformations.map(gameMode) {
        it.equals(GameMode.TIMER)
    }

    val matrixSize: LiveData<MatrixSize> = state.getLiveData(MATRIX_SIZE_KEY)

    private val _scores: MutableMap<MatrixSize, MutableLiveData<List<Score>>> = mutableMapOf()
    val scores: Map<MatrixSize, LiveData<List<Score>>>
        get() = _scores

    init {
        for (matrixSize in MatrixSize.values()) {
            _scores[matrixSize] = MutableLiveData()
        }
        uiScope.launch {
            if (!state.contains(RESTORING_STATE_KEY)) {
                state.set(RESTORING_STATE_KEY, true)
                val mostRecentGame = getMostRecentSavedGameFromDatabase()
                state.set(GAME_MODE_KEY, mostRecentGame.gameMode)
                state.set(MATRIX_SIZE_KEY, mostRecentGame.matrixSize)

                withContext(Dispatchers.IO) {
                    gameDao.deleteAllFinishedGamesWithNoNameExceptLatest()
                }
            }
            for (matrixSize in MatrixSize.values()) {
                _scores[matrixSize]!!.value = getScoresFromDatabase(matrixSize)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    private suspend fun getScoresFromDatabase(matrixSize: MatrixSize): List<Score> {
        return withContext(Dispatchers.IO) {
            gameDao.getScoresForScoreboardDisplayByGameModeAndMatrixSizeOrderByScoreDesc(
                state.get(GAME_MODE_KEY)!!,
                matrixSize
            )
        }
    }

    fun saveSelectedMatrixSize(matrixSize: MatrixSize) {
        state.set(MATRIX_SIZE_KEY, matrixSize)
    }

    fun onSelectTimerMode() {
        if (state.get<GameMode>(GAME_MODE_KEY) != GameMode.TIMER) {
            state.set(GAME_MODE_KEY, GameMode.TIMER)
            onGameModeChanged()
        }
    }

    fun onSelectLifeMode() {
        if (state.get<GameMode>(GAME_MODE_KEY) != GameMode.LIFE) {
            state.set(GAME_MODE_KEY, GameMode.LIFE)
            onGameModeChanged()
        }
    }

    private fun onGameModeChanged() {
        uiScope.launch {
            for (matrixSize in MatrixSize.values()) {
                _scores[matrixSize]!!.value = getScoresFromDatabase(matrixSize)
            }
        }
    }

    private suspend fun getMostRecentSavedGameFromDatabase(): Game {
        return withContext(Dispatchers.IO) {
            gameDao.getMostRecentFinishedGameWithName()
                ?: Game(
                    0L,
                    GameMode.LIFE,
                    MatrixSize.THREE,
                    null,
                    0,
                    0F,
                    "Player",
                    LocalDate.now(),
                    false
                )
        }
    }
}
