package edu.dhbw.switchsort.screens.about

import android.animation.LayoutTransition
import android.opengl.Visibility
import android.os.Bundle
import android.transition.AutoTransition
import android.transition.TransitionManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.navigation.fragment.findNavController
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentAboutBinding
import edu.dhbw.switchsort.databinding.FragmentGameBinding
import edu.dhbw.switchsort.screens.game.GameFragmentDirections
import edu.dhbw.switchsort.views.ExpandableCardView


class AboutFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val binding: FragmentAboutBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_about, container, false)

        binding.goBackButton.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.aboutList.layoutTransition.apply {
            enableTransitionType(LayoutTransition.CHANGING)
            disableTransitionType(LayoutTransition.APPEARING)
            disableTransitionType(LayoutTransition.CHANGE_APPEARING)
            disableTransitionType(LayoutTransition.DISAPPEARING)
        }

        requireActivity().packageManager.getPackageInfo(
                requireActivity().packageName,
                0
        )?.versionName?.let { binding.versionCard.setSecondaryText(it) }

        return binding.root
    }
}