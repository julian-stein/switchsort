package edu.dhbw.switchsort.screens.game

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.app.Application
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.media.AudioAttributes
import android.media.SoundPool
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import edu.dhbw.switchsort.R
import edu.dhbw.switchsort.databinding.FragmentGameBinding
import edu.dhbw.switchsort.datalayer.database.GameDatabase
import edu.dhbw.switchsort.enums.GameMode
import edu.dhbw.switchsort.util.SettingsHelper

class GameFragment : Fragment() {

    private lateinit var gameViewModel: GameViewModel
    private lateinit var binding: FragmentGameBinding
    private lateinit var pulsingAnimator: AnimatorSet
    private lateinit var blueToRedAnimator: AnimatorSet
    private val pulsingRedAnimator = AnimatorSet()

    private var soundPool: SoundPool? = null
    private var correctSound: Int = 0
    private var falseSound: Int = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        pulsingAnimator = AnimatorInflater.loadAnimator(
            context,
            R.animator.pulsing_circle_animator
        ) as AnimatorSet
        blueToRedAnimator =
            AnimatorInflater.loadAnimator(context, R.animator.blue_to_red) as AnimatorSet
        pulsingRedAnimator.playTogether(pulsingAnimator, blueToRedAnimator)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_game, container, false
        )

        val application = requireNotNull(this.activity).application
        val dao = GameDatabase.getInstance(application).gameDatabaseDao

        setupSoundPool(application)
        val viewModelFactory = GameViewModelFactory(dao, application)

        gameViewModel =
            ViewModelProvider(this, viewModelFactory)
                .get(GameViewModel::class.java)

        gameViewModel.resetCorrectClick()

        binding.lifecycleOwner = viewLifecycleOwner
        binding.gameViewModel = gameViewModel

        binding.matrixContainer.setGameViewModelReference(gameViewModel)

        binding.goBackButton.setOnClickListener {
            findNavController().navigateUp()
        }

        gameViewModel.apply {

            navigateToResults.observe(viewLifecycleOwner, {
                if (it) {
                    blueToRedAnimator.end()
                    pulsingRedAnimator.end()
                    findNavController().navigate(GameFragmentDirections.actionGameFragmentToResultsFragment())
                    doneNavigating()
                }
            })

            game.observe(viewLifecycleOwner, {
                game.value?.let { it1 -> binding.matrixContainer.setLayout(it1.matrixSize) }
                binding.matrixContainer.setValues(matrixFieldsValues.value!!)
                binding.matrixContainer.setIsHapticFeedbackEnabled()
                if (game.value?.gameMode == GameMode.TIMER) {
                    binding.timerContainer.visibility = View.VISIBLE
                    binding.lifeContainer.visibility = View.GONE
                    pulsingAnimator.setTarget(binding.timerContainer)
                    blueToRedAnimator.setTarget(binding.timerContainer.background as GradientDrawable)
                } else {
                    binding.lifeContainer.visibility = View.VISIBLE
                    binding.timerContainer.visibility = View.GONE
                    pulsingAnimator.setTarget(binding.lifeContainer)
                    blueToRedAnimator.setTarget(binding.lifeContainer.background as GradientDrawable)
                }
            })

            currentLifeDrawable.observe(viewLifecycleOwner, {
                pulsingRedAnimator.end()
                pulsingRedAnimator.start()
            })

            currentTimerMillis.observe(viewLifecycleOwner, {
                if (currentTimerMillis.value!! <= 10000) {
                    pulsingRedAnimator.end()
                    pulsingRedAnimator.start()
                }
            })

            matrixFieldsValues.observe(viewLifecycleOwner, {
                if (game.value != null) {
                    binding.matrixContainer.setValues(it)
                }
            })

            correctClick.observe(viewLifecycleOwner, {
                if (it != null) {
                    binding.matrixContainer.animateClicked(it)
                    if (it) {
                        soundPool?.play(correctSound, 1f, 1f, 1, 0, 1f)
                    } else {
                        soundPool?.play(falseSound, 1f, 1f, 1, 0, 1f)
                        if(game.value?.gameMode == GameMode.TIMER) {
                            pulsingRedAnimator.end()
                            pulsingRedAnimator.start()
                        }
                    }
                }
            })
        }

        return binding.root
    }

    override fun onPause() {
        super.onPause()
        gameViewModel.pauseGame()
    }

    override fun onResume() {
        super.onResume()
        gameViewModel.resumeGame()
    }

    override fun onStop() {
        super.onStop()
        gameViewModel.pauseGame()
        gameViewModel.storeGame()
    }

    override fun onDestroy() {
        super.onDestroy()
        soundPool?.release()
    }

    private fun setupSoundPool(application: Application) {
        if (context?.let {
                SettingsHelper(it).isAcousticFeedbackEnabled()
            } == true) {
            soundPool = SoundPool.Builder()
                .setMaxStreams(10)
                .setAudioAttributes(
                    AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_GAME)
                        .build()
                )
                .build()
            correctSound = soundPool?.load(application, R.raw.correct_sound, 1)!!
            falseSound = soundPool?.load(application, R.raw.false_sound, 1)!!
        }
    }
}