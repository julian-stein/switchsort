package edu.dhbw.switchsort.screens.scoreboard

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import edu.dhbw.switchsort.enums.MatrixSize

class ScoreboardViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return ScoreListFragment.newInstance(
            MatrixSize.getByValue(
                position + ScoreboardFragment.MATRIX_SIZE_OFFSET
            )
        )
    }

}