package edu.dhbw.switchsort.screens.game.lifescoring

import kotlin.math.pow

class Matrix5Scorer: LifeScorer() {
    override fun getLifeScore(clickTime: Long): Float {
        val x = ((((clickTime.toFloat()/1000) * 0.396 - 0.056).pow(-1)) - 0.174).toFloat()
        if (x > 3F) {
            return 3F
        } else if (x < 0F) {
            return 0F
        }
        return x
    }
}