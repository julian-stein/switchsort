package edu.dhbw.switchsort.screens.results

import android.text.Editable
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import edu.dhbw.switchsort.datalayer.database.GameDatabaseDao
import edu.dhbw.switchsort.datalayer.entities.Game
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class ResultsViewModel(
    private val gameDao: GameDatabaseDao,
    private val state: SavedStateHandle
) : ViewModel() {

    companion object {
        private const val NAME_INPUT_ENABLED_KEY = "name_input_enabled"
    }

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val _game: MutableLiveData<Game> = MutableLiveData()
    val game: Game?
        get() = _game.value

    val shareVisible: LiveData<Int> = Transformations.map(_game) {
        if(it == null) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

    val nameInputEnabled: LiveData<Boolean> = state.getLiveData(NAME_INPUT_ENABLED_KEY, true)

    val score: LiveData<String> = Transformations.map(_game) {
        String.format(Locale.US,"%.1f", it.score)
    }

    init {
        uiScope.launch {
            _game.value = withContext(Dispatchers.IO) {
                gameDao.getMostRecentFinishedGame()
            }
        }
    }

    fun storeGameWithNameIfSet(inputFieldValue: Editable?) {
        if (inputFieldValue != null && inputFieldValue.toString().trim().isNotEmpty()) {
            uiScope.launch {
                _game.value?.name = inputFieldValue.toString()
                withContext(Dispatchers.IO) {
                    gameDao.update(_game.value!!)
                }
                state.set(NAME_INPUT_ENABLED_KEY, false)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}