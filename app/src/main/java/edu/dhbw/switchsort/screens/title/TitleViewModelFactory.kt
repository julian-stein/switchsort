package edu.dhbw.switchsort.screens.title

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import edu.dhbw.switchsort.datalayer.database.GameDatabaseDao

class TitleViewModelFactory(
        private val dao: GameDatabaseDao) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TitleViewModel::class.java)) {
            return TitleViewModel(dao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}