package edu.dhbw.switchsort.util

import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager
import edu.dhbw.switchsort.R

/**
 * This class provides utilities to handle user preferences regarding UI mode.
 */
class ThemeHelper(private val context: Context) {

    private val themeKey = context.getString(R.string.themePreferencesKey)

    private val nightValue = context.getString(R.string.themePreferencesNightValue)
    private val lightValue = context.getString(R.string.themePreferencesLightValue)
    private val defaultValue = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) context.getString(R.string.themePreferencesDefaultValue) else lightValue

    /**
     * Get the UI mode saved in shared preferences by mapping defined string values to
     * AppCompatDelegate constants. Default value is mapped to not night on build versions prior to
     * Android Q.
     * @return AppCompatDelegate constant indicating the UI mode.
     */
    fun getThemeFromPreferences(): Int {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return when(sharedPreferences.getString(themeKey, defaultValue)) {
            nightValue -> AppCompatDelegate.MODE_NIGHT_YES
            lightValue -> AppCompatDelegate.MODE_NIGHT_NO
            else -> if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM else AppCompatDelegate.MODE_NIGHT_NO
        }
    }

    /**
     * Apply the UI mode saved in shared preferences.
     */
    fun applyThemeFromPreferences() {
        AppCompatDelegate.setDefaultNightMode(getThemeFromPreferences())
    }

    /**
     * Save an AppCompatDelegate constant to shared preferences by mapping the integer to defined
     * string values.
     * @param newTheme: Int the UI mode to save.
     */
    fun saveThemeToPreferences(newTheme: Int) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        with (sharedPreferences.edit()) {
            putString(themeKey, when(newTheme) {
                AppCompatDelegate.MODE_NIGHT_YES -> nightValue
                AppCompatDelegate.MODE_NIGHT_NO -> lightValue
                else -> defaultValue
            })
            apply()
        }
    }
}