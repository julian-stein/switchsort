package edu.dhbw.switchsort.util

import android.content.Context
import androidx.preference.PreferenceManager
import edu.dhbw.switchsort.R


/**
 * A helper class providing access to settings stored in shared preferences.
 */
class SettingsHelper(private val context: Context) {
    private val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun isHapticFeedbackEnabled(): Boolean {
        return sharedPreferences.getBoolean(
                context.getString(R.string.hapticFeedbackPreferencesKey),
                true
        )
    }

    fun setHapticFeedbackEnabled(enableHapticFeedback: Boolean) {
        with (sharedPreferences.edit()) {
            putBoolean(
                    context.getString(R.string.hapticFeedbackPreferencesKey),
                    enableHapticFeedback
            )
            apply()
        }
    }

    fun isAcousticFeedbackEnabled(): Boolean {
        return sharedPreferences.getBoolean(
                context.getString(R.string.acousticFeedbackPreferencesKey),
                true
        )
    }

    fun setAcousticFeedbackEnabled(enableAcousticFeedback: Boolean) {
        with (sharedPreferences.edit()) {
            putBoolean(
                    context.getString(R.string.acousticFeedbackPreferencesKey),
                    enableAcousticFeedback
            )
            apply()
        }
    }

    fun isBackgroundMusicEnabled(): Boolean {
        return sharedPreferences.getBoolean(
            context.getString(R.string.acousticFeedbackPreferencesKey),
            true
        )
    }

    fun setBackgroundMusicEnabled(enableBackgroundMusic: Boolean) {
        with (sharedPreferences.edit()) {
            putBoolean(
                context.getString(R.string.acousticFeedbackPreferencesKey),
                enableBackgroundMusic
            )
            apply()
        }
    }
}