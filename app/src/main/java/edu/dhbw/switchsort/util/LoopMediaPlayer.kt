package edu.dhbw.switchsort.util

import android.content.Context
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.media.MediaPlayer.OnPreparedListener

/**
 * @author Mattia Maestrini; https://stackoverflow.com/a/29883923
 *
 * Auto-converted to Kotlin with Android Studio 4.2.0;
 * Slightly adapted by Elias Mueller & Julian Stein.
 */
class LoopMediaPlayer private constructor(context: Context, resId: Int, volume: Float) {
    private var mContext: Context? = null
    private var mResId = 0
    private var mCurrentPlayer: MediaPlayer? = null
    private var mNextPlayer: MediaPlayer? = null
    private var mVolume: Float = 0F
    private fun createNextMediaPlayer() {
        mNextPlayer = MediaPlayer.create(mContext, mResId)
        mNextPlayer?.setVolume(mVolume, mVolume)
        mCurrentPlayer!!.setNextMediaPlayer(mNextPlayer)
        mCurrentPlayer!!.setOnCompletionListener(onCompletionListener)
    }

    private val onCompletionListener =
        OnCompletionListener { mediaPlayer ->
            mediaPlayer.release()
            mCurrentPlayer = mNextPlayer
            createNextMediaPlayer()
        }

    companion object {
        fun create(context: Context, resId: Int, volume: Float): LoopMediaPlayer {
            return LoopMediaPlayer(context, resId, volume)
        }
    }

    init {
        mContext = context
        mResId = resId
        mVolume = volume
        mCurrentPlayer = MediaPlayer.create(mContext, mResId)
        mCurrentPlayer?.setVolume(mVolume, mVolume)
        mCurrentPlayer?.setOnPreparedListener { mCurrentPlayer?.start() }
        createNextMediaPlayer()
    }

    fun start() {
        mCurrentPlayer?.start()
    }

    fun stop() {
        mCurrentPlayer?.stop()
    }

    fun release() {
        mCurrentPlayer?.stop()
        mNextPlayer?.stop()
        mCurrentPlayer?.release()
        mNextPlayer?.release()
        mCurrentPlayer = null
        mNextPlayer = null
    }
}