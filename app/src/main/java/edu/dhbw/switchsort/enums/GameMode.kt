package edu.dhbw.switchsort.enums

enum class GameMode {
    TIMER, LIFE
}