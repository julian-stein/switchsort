package edu.dhbw.switchsort.enums

enum class MatrixSize(val value: Int) {
    THREE(3),
    FOUR(4),
    FIVE(5);

    companion object {
        private val VALUES = values()
        fun getByValue(value: Int) = VALUES.first { it.value == value }
    }
}